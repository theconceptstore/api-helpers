<?php

return array(

    /*
	|--------------------------------------------------------------------------
	| Email address
	|--------------------------------------------------------------------------
	|
	| Provide an email address. This value can't be left empty, 'cause this
    | address is used when no log entry could be written to the database of
    | the api.
	|
	*/

    'email' => 'default@test.com'
);