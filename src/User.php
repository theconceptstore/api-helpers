<?php namespace Tcsehv\ApiHelpers;

use Tcsehv\ApiHelpers\Contracts\PrivilegeInterface;
use Tcsehv\ApiHelpers\Resource\Base;

class User extends Base implements PrivilegeInterface
{
    const ROLE_ADMINISTRATOR = 1;
    const ROLE_BRIGHTBOX_WEBSITE = 2;
    const ROLE_REGULAR_USER = 3;

    const FUNCTION_CREATE_PROFILE = 'createProfile';
    const FUNCTION_GET_PROFILE = 'getProfile';
    const FUNCTION_UPDATE_PASSWORD = 'updatePassword';
    const FUNCTION_GENERATE_TOKEN = 'generateToken';
    const FUNCTION_ENABLE_USER = 'enableUser';
    const FUNCTION_DISABLE_USER = 'disableUser';
    const FUNCTION_ALL_USERS = 'allUsers';
    const FUNCTION_UPDATE_USER = 'updateUser';

    /**
     * @param string $username
     * @param string $password
     * @param int $roleId
     * @return string
     */
    public function createProfile($username, $password, $roleId)
    {
        if ($this->validateMethod(self::FUNCTION_CREATE_PROFILE)) {
            $response = $this->apiClient->endpoint('users')
                ->option('username', $username)
                ->option('password', $password)
                ->option('role', $roleId)
                ->post();
        } else {
            $response = $this->setupResponse(403, 'Forbidden');
        }
        return $this->parseResponse($response);
    }

    /**
     * Retrieve profile information
     * If the property is left blank, the current user will be returned
     *
     * @param null $id
     * @param null $name
     * @param bool $withRemovedItems
     * @return string
     */
    public function getProfile($id = null, $name = null, $withRemovedItems = false)
    {
        if ($this->validateMethod(self::FUNCTION_GET_PROFILE)) {
            $response = $this->apiClient->endpoint('users/profile')
                ->option('id', $id)
                ->option('name', $name)
                ->option('withRemoved', $withRemovedItems)
                ->get();
        } else {
            $response = $this->setupResponse(403, 'Forbidden');
        }
        return $this->parseResponse($response);
    }

    /**
     * Update password for a specific account
     *
     * @param int $id
     * @param string $password
     * @param string $passwordConfirmation
     * @return string
     */
    public function updatePassword($id, $password, $passwordConfirmation)
    {
        if ($this->validateMethod(self::FUNCTION_UPDATE_PASSWORD)) {
            $response = $this->apiClient->endpoint('users/update_password')
                ->option('password', $password)
                ->option('password_confirmation', $passwordConfirmation)
                ->option('id', $id)
                ->post();
        } else {
            $response = $this->setupResponse(403, 'Forbidden');
        }
        return $this->parseResponse($response);
    }

    /**
     * Create token for the specified user
     *
     * @param string $username
     * @param string $password
     * @return string
     */
    public function generateToken($username, $password)
    {
        if ($this->validateMethod(self::FUNCTION_GENERATE_TOKEN)) {
            $response = $this->apiClient->endpoint('token')
                ->option('username', $username)
                ->option('password', $password)
                ->post();
        } else {
            $response = $this->setupResponse(403, 'Forbidden');
        }
        return $this->parseResponse($response);
    }

    /**
     * Enable user, so this item can start using the api
     *
     * @param int $id
     * @return string
     */
    public function enableUser($id)
    {
        if ($this->validateMethod(self::FUNCTION_ENABLE_USER)) {
            $response = $this->manageUserAccess($id, true);
        } else {
            $response = $this->setupResponse(403, 'Forbidden');
        }
        return $this->parseResponse($response);
    }

    /**
     * Disable user, which was enabled
     *
     * @param $id
     * @return string
     */
    public function disableUser($id)
    {
        if ($this->validateMethod(self::FUNCTION_DISABLE_USER)) {
            $response = $this->manageUserAccess($id, false);
        } else {
            $response = $this->setupResponse(403, 'Forbidden');
        }
        return $this->parseResponse($response);
    }

    /**
     * Retrieve list of all available users
     *
     * @param int $page
     * @param bool $showInactive
     * @return string
     */
    public function all($page = null, $showInactive = false){
        if($this->validateMethod(self::FUNCTION_ALL_USERS)) {
            $response = $this->apiClient->endpoint('users')
                ->option('inactive', $showInactive);

            // Apply pagination, if available
            if(!empty($page)) {
                $response->option('page', $page);
            }
            $response = $response->get();
        } else {
            $response = $this->setupResponse(403, 'Forbidden');
        }
        return $this->parseResponse($response);
    }

    /**
     * Update user data
     *
     * @param int $userId
     * @param string $username
     * @param int $websitesId
     * @param int $rolesId
     * @return string
     */
    public function updateUser($userId, $username, $websitesId, $rolesId)
    {
        if($this->validateMethod(self::FUNCTION_UPDATE_USER)) {
            $response = $this->apiClient->endpoint('users/update_user')
                ->option('id', intval($userId))
                ->option('username', $username)
                ->option('websites_id', intval($websitesId))
                ->option('roles_id', intval($rolesId))
                ->patch();
        } else {
            $response = $this->setupResponse(403, 'Forbidden');
        }
        return $response;
    }

    /**
     * Manage access for the specified user
     *
     * @param int $id
     * @param bool $enableUser
     * @return string
     */
    private function manageUserAccess($id, $enableUser)
    {
        $response = $this->apiClient->endpoint('users/manage_access')
            ->option('id', intval($id))
            ->option('enable', $enableUser)
            ->post();

        return $response;
    }
}