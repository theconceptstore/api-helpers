<?php namespace Tcsehv\ApiHelpers;

use Tcsehv\ApiHelpers\Contracts\PrivilegeInterface;
use Tcsehv\ApiHelpers\Resource\Base;

class Search extends Base implements PrivilegeInterface
{

    const FUNCTION_CRAWL_DOCUMENTS = 'crawlDocuments';
    const FUNCTION_CREATE = 'createSearchIndex';
    const FUNCTION_CREATE_TYPE_IN_INDEX = 'createTypeInIndex';
    const FUNCTION_DELETE_INDEX = 'deleteIndex';
    const FUNCTION_DELETE_TYPE_IN_INDEX = 'deleteTypInIndex';
    const FUNCTION_SEARCH_QUERY = 'searchQuery';
    const FUNCTION_RESTORE_INDEX = 'restoreIndex';
    const FUNCTION_SEARCH_INDEX = 'searchIndex';
    const FUNCTION_RETRIEVE_INDEX = 'retrieveIndex';
    const FUNCTION_UPDATE_INDEX = 'updateIndex';

    /**
     * @param string $url
     * @param string $index
     * @param string $type
     * @param bool $followLinks
     * @param bool $linkMatch
     * @param bool $excludeMatch
     * @return null|string
     */
    public function crawlDocuments($url, $index, $type, $followLinks = true, $linkMatch = false, $excludeMatch = false)
    {
        $response = null;

        if ($this->validateMethod(self::FUNCTION_CRAWL_DOCUMENTS)) {
            $response = $this->apiClient->endpoint('search/document/crawl')
                ->option('url', $url)
                ->option('index', $index)
                ->option('type', $type)
                ->option('follow_links', $followLinks)
                ->option('link_match', $linkMatch)
                ->option('exclude_match', $excludeMatch)
                ->post();
        }
        return $response;
    }

    /**
     * @param string $label
     * @param int $userId
     * @param int $shards
     * @param int $replicas
     * @param string $languages
     * @return null|string
     */
    public function createSearchIndex($label, $userId, $shards = 1, $replicas = 0, $languages = 'English')
    {
        $response = null;

        if ($this->validateMethod(self::FUNCTION_CREATE)) {
            $response = $this->apiClient->endpoint('search/index')
                ->option('label', $label)
                ->option('shards', $shards)
                ->option('replicas', $replicas)
                ->option('user_id', $userId)
                ->option('languages', $languages)
                ->post();
        }
        return $response;
    }

    /**
     * @param int $id
     * @param string $type
     * @param string $name
     * @param string $languages
     * @return null|string
     */
    public function createTypeInIndex($id, $type = 'web', $name = 'search', $languages = 'English')
    {
        $response = null;

        if ($this->validateMethod(self::FUNCTION_CREATE_TYPE_IN_INDEX)) {
            $response = $this->apiClient->endpoint('search/index/' . $id . '/type/create')
                ->option('type', $type)
                ->option('name', $name)
                ->option('languages', $languages)
                ->post();
        }
        return $response;
    }

    public function deleteIndex($id)
    {
        $response = null;

        if ($this->validateMethod(self::FUNCTION_DELETE_INDEX)) {
            $response = $this->apiClient->endpoint('/serach/index/' . $id)
                ->delete();
        }
        return $response;
    }

    /**
     * @param string $id
     * @param string $name
     * @return null|string
     */
    public function deleteTypeInIndex($id, $name)
    {
        $response = null;

        if ($this->validateMethod(self::FUNCTION_DELETE_TYPE_IN_INDEX)) {
            $response = $this->apiClient->endpoint('/search/index/' . $id . '/type/' . $name)
                ->delete();
        }
        return $response;
    }

    /**
     * @param string $id
     * @param string $type
     * @param string $query
     * @param string $domain
     * @param float $minScore
     * @param int $itemsPerPage
     * @param int $maxResults
     * @param int $currentPage
     * @param float $fuzziness
     * @return null|string
     */
    public function searchQuery($id, $type, $query, $domain, $minScore = 0.3, $itemsPerPage = 25, $maxResults = 1000, $currentPage = 1, $fuzziness = 1.0)
    {
        $response = null;

        if ($this->validateMethod(self::FUNCTION_SEARCH_QUERY)) {
            $response = $this->apiClient->endpoint('/search/query')
                ->option('index', $id)
                ->option('type', $type)
                ->option('q', $query)
                ->option('domain', $domain)
                ->option('min_score', $minScore)
                ->option('items_per_page', $itemsPerPage)
                ->option('max_results', $maxResults)
                ->option('page', $currentPage)
                ->option('fuzziness', $fuzziness)
                ->get();
        } else {
            $response = [];
        }
        return $response;
    }

    /**
     * @param int $id
     * @return null|string
     */
    public function restoreIndex($id)
    {
        $response = null;

        if ($this->validateMethod(self::FUNCTION_RESTORE_INDEX)) {
            $response = $this->apiClient->endpoint('/search/index/' . $id . '/restore')
                ->put();
        }

        return $response;
    }

    /**
     * @param $users
     * @param int $itemsPerPage
     * @param string $order
     * @param string $sort
     * @return null|string
     */
    public function searchIndex($users, $itemsPerPage = 25, $order = 'username', $sort = 'asc')
    {
        $response = null;

        if ($this->validateMethod(self::FUNCTION_SEARCH_INDEX)) {
            $response = $this->apiClient->endpoint('/search/index')
                ->option('users', $users)
                ->option('items_per_page', $itemsPerPage)
                ->option('order', $order)
                ->option('sort', $sort)
                ->get();
        }

        return $response;
    }

    /**
     * @param int $id
     * @return null|string
     */
    public function retrieveIndex($id)
    {
        $response = null;

        if ($this->validateMethod(self::FUNCTION_RETRIEVE_INDEX)) {
            $response = $this->apiClient->endpoint('/search/index/' . $id)
                ->get();
        }
        return $response;
    }

    /**
     * @param mixed $id
     * @param string $label
     * @param int $userId
     * @param bool $active
     * @return null|string
     */
    public function updateIndex($id, $label, $userId, $active = true)
    {
        $response = null;

        if ($this->validateMethod(self::FUNCTION_UPDATE_INDEX)) {
            $response = $this->apiClient->endpoint('/search/index/' . $id)
                ->option('label', $label)
                ->option('user_id', $userId)
                ->option('active', $active)
                ->put();
        }
        return $response;
    }
}