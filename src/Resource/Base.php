<?php namespace Tcsehv\ApiHelpers\Resource;

use Monolog\Logger;
use Tcsehv\ApiHelpers\Contracts\HelperInterface;
use Tcsehv\ApiHelpers\Log;
use Tcsehv\JwtApiClient\Resources\ApiClient;

class Base implements HelperInterface
{
    const HTTP_RESPONSE_CODE_OK = 200;

    /**
     * @var ApiClient
     */
    protected $apiClient;

    /**
     * @var bool
     */
    protected $debug;

    /**
     * @var array
     */
    protected $allowedMethods;

    /**
     * Every client needs an instance of the API client. This class is responsible for creating the connection to the API.
     * When debug is true, errors will be shown. If not, the logger will be activated.
     *
     * @param ApiClient $apiClient
     * @param bool $debug
     */
    public function __construct(ApiClient $apiClient, $debug = false)
    {
        $this->apiClient = $apiClient;
        $this->debug = $debug;

        $this->setAllowedMethods(null);
    }

    /**
     * Handle response for throwing exceptions if necessary
     *
     * @param string $response
     * @return string
     */
    public function parseResponse($response)
    {
        // Convert json to stdClass
        $response = json_decode($response);
        
        // If status code isn't set, respond current data
        if(empty($response->status_code) || (!empty($response->status_code) && $response->status_code == self::HTTP_RESPONSE_CODE_OK)) {
            return $response;
        } else {
            // Determine which type of response has to be returned, depending on the environment type
            if($this->debug) {
                throw new \RuntimeException('The following status code was returned: ' . $response->status_code, $response->message);
            } else {
                $this->createLogEntry($response);

                return $response;
            }
        }
    }

    /**
     * @return boolean
     */
    public function isDebug()
    {
        return $this->debug;
    }

    /**
     * Build list with allowed methods
     *
     * @param string|array $methods
     */
    public function setAllowedMethods($methods)
    {

        if(count(func_get_args()) > 1) {
            $methods = func_get_args();
        } else if (is_string($methods)) {
            $methods = array($methods);
        }

        $this->allowedMethods = [];

        $parentClass = get_called_class();
        $reflection = new \ReflectionClass($parentClass);
        $constants = $reflection->getConstants();

        foreach($constants as $key => $value) {
            if(strpos($key, 'FUNCTION_') === false) {
                unset($constants[$key]);
            }
        }

        if($methods === null) {
            foreach($constants as $method) {
                $this->allowedMethods[] = $method;
            }
        } else {
            foreach($methods as $method) {
                if(in_array($method, $constants)) {
                    $this->allowedMethods[] = $method;
                }
            }
        }
    }

    /**
     * Check if the user is allowed to access the provided method
     *
     * @param string $method
     * @return bool
     */
    protected function validateMethod($method)
    {

        if(!empty($this->allowedMethods)) {
            if(in_array($method, $this->allowedMethods)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Create simple response
     *
     * @param int $httpCode
     * @param string $message
     * @return string
     */
    protected function setupResponse($httpCode, $message) {
        $httpCode = !is_numeric($httpCode) ? intval($httpCode) : $httpCode;

        $response = new \stdClass();
        $response->status_code = $httpCode;
        $response->message = $message;

        header('HTTP/1.1 ' . $httpCode . ' ' . $message);
        return json_encode($response);
    }

    /**
     * Setup basic context to retrieve required information from the client
     *
     * @return array
     */
    private function setupContext() {
        return array(
            'user_agent' => $_SERVER['HTTP_USER_AGENT'],
            'ip' => $_SERVER['REMOTE_ADDR'],
            'uri' => $_SERVER['REQUEST_URI'],
            'host' => $_SERVER['HTTP_HOST'],
            'request_method' => $_SERVER['REQUEST_METHOD'],
            'referer' => $_SERVER['HTTP_REFERER'],
            'remote_port' => $_SERVER['REMOTE_PORT']
        );
    }

    /**
     * Create log entry and save it into the remote database
     *
     * @param \stdClass $response
     * @return array
     */
    private function createLogEntry($response)
    {
        \Log::alert($response->status_code . ': ' . $response->message, $this->setupContext());
    }
}