<?php namespace Tcsehv\ApiHelpers\Resource;

use Illuminate\Support\Facades\File;
use Symfony\Component\Finder\SplFileInfo;

class Permissions
{
    /**
     * @var string
     */
    protected static $privilegeInterface = 'Tcsehv\\ApiHelpers\\Contracts\\PrivilegeInterface';

    /**
     * @var array
     */
    protected $permissions;

    /**
     * @var array
     */
    protected $allowedLevels;

    public function __construct()
    {
        // Get listing of files which have to been checked
        $files = $this->loadFiles();
        $permissions = array();

        /** @var SplFileInfo $file */
        foreach ($files as $file) {
            $constants = array();
            
            // Check file extension
            if ($file->getExtension() === 'php') {
                $className = 'Tcsehv\\ApiHelpers\\' . str_replace('.php', '', $file->getFilename());

                // While class exists and named interface was used, process constants
                if (class_exists($className)) {
                    $reflection = new \ReflectionClass($className);

                    if ($reflection->implementsInterface(self::$privilegeInterface)) {
                        $constants = $this->parseConstants($reflection->getConstants(), $reflection->getShortName());
                    }
                }
            }
            $permissions = array_merge($permissions, $constants);
        }
        $this->permissions = $permissions;
    }

    /**
     * @return array
     */
    public function get()
    {
        return $this->permissions;
    }

    /**
     * Check if the provided role is available for use
     *
     * @param string $role
     * @return bool
     * @throws \RuntimeException
     */
    public function isValid($role)
    {
        if(!empty($this->allowedLevels)) {
            if (is_string($role)) {
                return in_array($role, $this->allowedLevels);
            }
            return false;
        }
        throw new \RuntimeException('$allowedLevels must be a populated array');
    }

    /**
     * @param string|array $method
     * @param string $_ [optional]
     * @return array
     */
    public static function decodeAllowedMethods($method, $_ = null)
    {
        $result = array();

        if(!is_array($method)) {
            $methods = (array) $method;
        }

        foreach($methods as $method => $value) {
            // Explode value and remove first entry
            $splitMethod = explode('-', $method);
            unset($splitMethod[0]);

            // Make the first character of every word in the array uppercase
            $splitMethod = array_map('ucwords', $splitMethod);

            $result[] = lcfirst(implode('', $splitMethod));
        }
        return $result;
    }

    /**
     * @param array $allowedLevels
     */
    public function setAllowedLevels(array $allowedLevels)
    {
        $this->allowedLevels = $allowedLevels;
    }

    /**
     * Retrieve files list from the dependency
     *
     * @return array
     */
    protected function loadFiles()
    {
        $files = \File::allFiles( __DIR__ . '/../');

        return $files;
    }

    /**
     * Get list of required constants, which are containing the different function names
     *
     * @param array $constants
     * @param string $className
     * @return array
     */
    protected function parseConstants(array $constants, $className)
    {
        $result = array();

        if (count($constants) > 0) {
            foreach ($constants as $key => $value) {
                if (strpos($key, 'FUNCTION_') !== false) {
                    $newKey = strtolower($className . '-' . preg_replace('/([A-Z])/x', '-$0', $value));

                    $result[$newKey] = false;
                }
            }
        }
        return $result;
    }
}