<?php namespace Tcsehv\ApiHelpers;

use Tcsehv\ApiHelpers\Contracts\PrivilegeInterface;
use Tcsehv\ApiHelpers\Resource\Base;

class Role extends Base implements PrivilegeInterface
{
    const FUNCTION_ALL_ROLES = 'allRoles';
    const FUNCTION_FIND = 'findRole';
    const FUNCTION_UPDATE_PERMISSIONS = 'updatePermissions';
    const FUNCTION_FIND_ROLE_BY_USER = 'findRoleByUser';

    /**
     * Retrieve all available roles
     *
     * @return string
     */
    public function all()
    {
        if ($this->validateMethod(self::FUNCTION_ALL_ROLES)) {
            $response = $this->apiClient->endpoint('roles')
                ->get();
        } else {
            $response = $this->setupResponse(403, 'Forbidden');
        }
        return $this->parseResponse($response);
    }

    /**
     * Find specific role
     *
     * @param int $id
     * @return string
     */
    public function find($id)
    {
        if ($this->validateMethod(self::FUNCTION_FIND)) {
            $response = $this->apiClient->endpoint('roles/find')
                ->option('id', intval($id))
                ->get();
        } else {
            $response = $this->setupResponse(403, 'Forbidden');
        }
        return $this->parseResponse($response);
    }

    /**
     * Find role for specific user
     *
     * @param int $userId
     * @return string
     */
    public function findRoleByUser($userId){
        if ($this->validateMethod(self::FUNCTION_FIND_ROLE_BY_USER)) {
            $response = $this->apiClient->endpoint('roles/role_for_user')
                ->option('id', intval($userId))
                ->get();
        } else {
            $response = $this->setupResponse(403, 'Forbidden');
        }
        return $this->parseResponse($response);
    }

    /**
     * Save permissions for the provided role identifier
     *
     * @param int $roleId
     * @param string|array $permissions
     * @return string
     */
    public function updatePermissions($roleId, $permissions){
        if(is_array($permissions)) {
            // Remove empty values and serialize
            $permissions = serialize(array_filter($permissions));
        }

        if($this->validateMethod(self::FUNCTION_UPDATE_PERMISSIONS)) {
            $response = $this->apiClient->endpoint('roles/update_permissions')
                ->option('id', $roleId)
                ->option('permissions', $permissions)
                ->post();
        } else {
            $response = $this->setupResponse(403, 'Forbidden');
        }
        return $this->parseResponse($response);
    }

}