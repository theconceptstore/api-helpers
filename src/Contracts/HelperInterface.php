<?php namespace Tcsehv\ApiHelpers\Contracts;

use Tcsehv\JwtApiClient\Resources\ApiClient;

interface HelperInterface
{

    /**
     * Every client needs an instance of the API client. This class is responsible for creating the connection to the API.
     * When debug is true, errors will be shown. If not, the logger will be activated.
     *
     * @param ApiClient $apiClient
     * @param bool $debug
     */
    public function __construct(ApiClient $apiClient, $debug = false);
}