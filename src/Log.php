<?php namespace Tcsehv\ApiHelpers;

use Monolog\Logger;
use Tcsehv\ApiHelpers\Contracts\PermissionInterface;
use Tcsehv\ApiHelpers\Contracts\PrivilegeInterface;
use Tcsehv\ApiHelpers\Resource\Base;

class Log extends Base implements PrivilegeInterface
{

    const FUNCTION_CREATE = 'createLog';
    const FUNCTION_SEARCH = 'searchLog';
    const FUNCTION_SEARCH_ALL = 'searchAllLogs';
    const FUNCTION_FIND_SINGLE_ITEM = 'findSingleItem';

    /**
     * Create a new log entry
     *
     * @param string $channel
     * @param string $message
     * @param string $level
     * @param string $levelName
     * @param string $formatted
     * @param \DateTime $dateTime
     * @param array $context
     * @return string|null
     */
    public function create($channel, $message, $level, $levelName, $formatted, \DateTime $dateTime, array $context)
    {
        $response = null;

        if ($this->validateMethod(self::FUNCTION_CREATE)) {
            $response = $this->apiClient
                ->endpoint('log')
                ->option('channel', $channel)
                ->option('message', $message)
                ->option('level', $level)
                ->option('level_name', $levelName)
                ->option('formatted', $formatted)
                ->option('created_at', $dateTime->format('Y-m-d H:i:s'))
                ->option('context', $context)
                ->post();
        }
        return $response;
    }

    /**
     * Retrieve list with logs, which can be specified using the parameters
     *
     * @param null|string $term
     * @param int $limit
     * @param int $page
     * @param string $sort
     * @param string $order
     * @param int $minLevel
     * @param int $maxLevel
     * @return string|null
     */
    public function search($term = null, $limit = 25, $page = 1, $sort = 'datetime', $order = 'desc', $minLevel = Logger::DEBUG, $maxLevel = Logger::EMERGENCY)
    {
        $response = null;

        if ($this->validateMethod(self::FUNCTION_SEARCH)) {
            $response = $this->apiClient
                ->endpoint('log/search')
                ->option('term', $term)
                ->option('limit', $limit)
                ->option('page', $page)
                ->option('sort', $sort)
                ->option('order', $order)
                ->option('minLevel', $minLevel)
                ->option('maxLevel', $maxLevel)
                ->get();
        }
        return $response;
    }

    /**
     * @param int $limit
     * @param int $page
     * @param int $minLevel
     * @param int $maxLevel
     * @return null|string
     */
    public function searchAll($limit = 25, $page = 1, $minLevel = Logger::DEBUG, $maxLevel = Logger::EMERGENCY)
    {
        $response = null;

        if ($this->validateMethod(self::FUNCTION_SEARCH_ALL)) {
            $response = $this->apiClient
                ->endpoint('log/search')
                ->option('limit', $limit)
                ->option('min_level', $minLevel)
                ->option('max_level', $maxLevel)
                ->option('page', $page)
                ->get();
        }
        return $response;
    }

    public function findSingleItem($id)
    {
        $response = null;

        if($this->validateMethod(self::FUNCTION_FIND_SINGLE_ITEM)) {
            $response = $this->apiClient->endpoint('log/find')
                ->option('id', $id)
                ->get();
        }
        return $response;
    }
}