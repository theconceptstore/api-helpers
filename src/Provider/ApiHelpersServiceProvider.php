<?php namespace Tcsehv\ApiHelpers\Provider;

use Illuminate\Support\ServiceProvider;

class ApiHelpersServiceProvider extends ServiceProvider {

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->package('theconceptstore/api-helpers', 'api-helpers', __DIR__ . '/../');
    }
}