<?php namespace Tcsehv\ApiHelpers\Provider;

/**
 * Simple provider to set email addresses, used in the DefaultLogHandler
 *
 * Class BrightBoxProvider
 * @package Tcsehv\ApiHelpers\Provider
 */
class BrightBoxProvider
{
    /**
     * @var string
     */
    private $mailFrom;

    /**
     * @var string
     */
    private $mailTo;

    /**
     * @param string $mailFrom
     * @param string $mailTo
     */
    public function __construct($mailFrom, $mailTo)
    {
        $this->mailFrom = $mailFrom;
        $this->mailTo = $mailTo;
    }

    /**
     * @return string
     * @throws \InvalidArgumentException
     */
    public function getMailFrom()
    {
        if (filter_var($this->mailFrom, FILTER_VALIDATE_EMAIL)) {
            return $this->mailFrom;
        }
        throw new \InvalidArgumentException('$mailFrom is not valid');
    }

    /**
     * @return string
     * @throws \InvalidArgumentException
     */
    public function getMailTo()
    {
        if (filter_var($this->mailTo, FILTER_VALIDATE_EMAIL)) {
            return $this->mailTo;
        }
        throw new \InvalidArgumentException('$mailTo is not valid');
    }


}