<?php namespace Tcsehv\ApiHelpers;

use Tcsehv\ApiHelpers\Contracts\PrivilegeInterface;
use Tcsehv\ApiHelpers\Resource\Base;

class Website extends Base implements PrivilegeInterface
{
    const FUNCTION_ALL_WEBSITES = 'allWebsites';

    public function all()
    {
        if ($this->validateMethod(self::FUNCTION_ALL_WEBSITES)) {
            $response = $this->apiClient->endpoint('websites')
                ->get();
        } else {
            $response = $this->setupResponse(403, 'Forbidden');
        }
        return $this->parseResponse($response);
    }

}