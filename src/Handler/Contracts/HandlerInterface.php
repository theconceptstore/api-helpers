<?php namespace Tcsehv\ApiHelpers\Handler\Contracts;

use Monolog\Logger;
use Tcsehv\ApiHelpers\Provider\BrightBoxProvider;
use Tcsehv\JwtApiClient\Resources\ApiClient;

interface HandlerInterface {

    /**
     * Initialize client, eventually a BrightBox-provider and specify minimum debugging level and bubble
     *
     * @param ApiClient $apiClient
     * @param null|BrightBoxProvider $provider
     * @param bool|int $level
     * @param bool $bubble
     */
    public function __construct(ApiClient $apiClient, BrightBoxProvider $provider = null, $level = Logger::DEBUG, $bubble = true);

}