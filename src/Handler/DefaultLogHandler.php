<?php namespace Tcsehv\ApiHelpers\Handler;

use Monolog\Handler\AbstractProcessingHandler;
use Monolog\Logger;
use Tcsehv\ApiHelpers\Handler\Contracts\HandlerInterface;
use Tcsehv\ApiHelpers\Log;
use Tcsehv\ApiHelpers\Provider\BrightBoxProvider;
use Tcsehv\JwtApiClient\Resources\ApiClient;

class DefaultLogHandler extends AbstractProcessingHandler implements HandlerInterface {

    /**
     * @var Log
     */
    protected $log;

    /**
     * @var null|BrightBoxProvider
     */
    protected $provider;

    /**
     * @param ApiClient $apiClient
     * @param null|BrightBoxProvider $provider
     * @param bool|int $level
     * @param bool $bubble
     */
    public function __construct(ApiClient $apiClient, BrightBoxProvider $provider = null, $level = Logger::DEBUG, $bubble = true)
    {
        $this->log = new Log($apiClient);
        $this->log->setAllowedMethods(array(Log::FUNCTION_CREATE));

        $this->provider = $provider;

        parent::__construct($level, $bubble);
    }

    /**
     * Writes the record down to the log of the implementing handler
     *
     * @param  array $record
     * @return void
     */
    protected function write(array $record)
    {
        
        $response = $this->log->create($record['channel'], $record['message'], $record['level'], $record['level_name'], $record['formatted'], $record['datetime'], $record['context']);

        if(empty($response)) {
            $data = [
                'content' => $record['message'],
                'level_name' => $record['level_name'],
                'datetime' => $record['datetime']->format('Y-m-d H:i:s'),
                'context' => print_r($record['context'], true),
            ];

            \Mail::send('mail.error', $data, function($mail) {
                $mail->from($this->getMailFrom());
                $mail->to($this->getMailTo());
                $mail->subject('Error during logging to the API');
            });
        }
        return $response;
    }

    /**
     * Retrieve email address to mail from
     *
     * @return string
     */
    private function getMailFrom()
    {
        if(!empty($this->provider)) {
            return $this->provider->getMailFrom();
        }
        return \Config::get('api-helpers::mail_from');
    }

    /**
     * Retrieve email address to mail to
     *
     * @return string
     */
    private function getMailTo()
    {
        if(!empty($this->provider)) {
            return $this->provider->getMailTo();
        }
        return \Config::get('api-helpers::mail_to');
    }
}