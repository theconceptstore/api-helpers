# HOW-TO #

Below, you will find some specifications for start using this dependency. This dependency will sent an email to a provided address, when the package is unable to write something to the log-endpoint in our API.

After installation, you can decide to use to different providers. For Laravel, there is a regular provider available. You have to add this provider to the providers-array in your app/config/app.php. 

```
'Tcsehv\ApiHelpers\Provider\ApiHelpersServiceProvider',
```

It's also required to publish the configuration file using the following line:

```
php artisan config:publish theconceptstore/api-helpers
```

When you are using another Framework, you have to initialize the provider by yourself using the following line:
```
$provider = new Tcsehv\ApiHelpers\Provider\BrightBoxProvider('name@test.com');
```